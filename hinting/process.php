<?php 
    include_once("./data.php");

    $array_data = [];

    if(!empty($_GET['data'])) {
        foreach (getDataArray() as $index => $value) {
            if(strpos(strtolower($value), strtolower($_GET['data'])) !== false) {
                array_push($array_data, $value);
            }
        }
        echo json_encode($array_data);
    }

?>