window.onload = () => {
    let text_input = document.querySelector('#input-text');

    text_input.addEventListener('input', (e) => {
        getData(e.target.value);
    });
    
    function getData(value) {
        fetch(`http://localhost:2025/hinting/process.php/?data=${value}`).then((res) => {
            res.text().then((data) => {
                createDataUI(data);
            })
        })
    }

    function createDataUI(data) {
        if(data !== "") {
            let arr_data = JSON.parse(data);
            $('div').remove('.alltext');

            for (let index = 0; index < arr_data.length; index++) {
                $('#searchtext').append(`<div class="row alltext border border-secondary m-1" style="height: 25px"><p class="text-center">${arr_data[index]}</p></div>`);
            }
        } else {
            $('div').remove('.alltext');
        }
    }
}