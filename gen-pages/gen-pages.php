<html>
<style>
body {padding:0px;margin:0px;}
.class1 {color:yellow;background-color:brown;width:100%;}
.class2 {color:orange;background-color:grey;width:100%;}
.class3 {color:blue;background-color:lightblue;width:100%;}
.class4 {color:grey;background-color:black;width:100%;}
</style>
<body>
<?php
function build_object ($nb) {
  $main_content = ""; // should be empty on init
    $list_obj = ["<div> </div>","<span> </span>","<p> </p>"];
    $list_class = ["class1","class2","class3","class4"];
    $list_content = ["lorem ipsum"]; // or generate with fct 
    // take object + class + content
    for ($i=0;$i<$nb;$i++) {
        $myobj = $list_obj[array_rand($list_obj, 1)];
        $mycls = $list_class[array_rand($list_class, 1)];
        $splitted = explode(' ',$myobj);
        $start = $splitted[0];
        $end = $splitted[1];
        $newstart = substr_replace($start, ' class="'.$mycls.'"', -1, 0);
        $final_obj = $newstart.$list_content[0].$end ;
      // ajoute l'objet dans la liste principale 
      $main_content .= $final_obj;
    }
    // add to main content 
    return $main_content ;
} // end of fct 
$affichage = build_object(5);
echo $affichage ; // affiche la liste principale 
?>
</body>
</html>